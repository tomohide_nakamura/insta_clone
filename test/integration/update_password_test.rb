require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end

  test "update password" do
    get edit_password_path(@user)
    assert_template 'users/edit_password'
    # 無効なパスワードとパスワード確認
    patch password_path(@user),
          params: { user: { password: 'password',
                            new_password:              "foobaz",
                            new_password_confirmation: "barquux" } }
    assert_not flash.empty?
    # パスワードが空
    patch password_path(@user),
          params: { user: { password: 'password',
                            new_password:              "",
                            new_password_confirmation: "" } }
    assert_not flash.empty?
    # 有効なパスワードとパスワード確認
    patch password_path(@user),
          params: { user: { password: 'password',
                            new_password:              "foobaz",
                            new_password_confirmation: "foobaz" } }
    assert_not flash.empty?
    assert_redirected_to @user
  end
end