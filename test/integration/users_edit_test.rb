require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
#    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              user_name: "" }}
#    assert_template 'users/edit'
  end
  
   test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
#    assert_template 'users/edit'
    name  = "Foo Bar"
    user_name = "hoge"
    patch user_path(@user), params: { user: { name:  name,
                                              user_name: user_name,
                                              email: "",
                                              website: "",
                                              introduce: ""}}
    assert_not flash.empty?
#    assert_redirected_to @user
    @user.reload
#    assert_equal name,  @user.name
#    assert_equal user_name, @user.user_name
  end
end