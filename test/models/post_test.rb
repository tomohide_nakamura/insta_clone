require 'test_helper'

class PostTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @post = @user.posts.new(picture: "aaa")
  end

  test "should be valid" do
#    assert @post.valid?
  end

  test "user id should be present" do
    @post.user_id = nil
    assert_not @post.valid?
  end
  
   test "picture should be present" do
    @post.picture = ""
    assert_not @post.valid?
  end

  test "picture_title should be at most 140 characters" do
    @post.picture_title = "a" * 141
    assert_not @post.valid?
  end
  
   test "order should be most recent first" do
    assert_equal posts(:most_recent), Post.first
  end
end