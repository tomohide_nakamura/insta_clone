class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_many :favorites
  has_many :users, through: :favorites
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :picture, presence: true
  validates :picture_title, length: { maximum: 140 }
  validate  :picture_size
  
  def self.search(search)
    return Post.all unless search
    Post.where(['picture_title LIKE ?', "%#{search}%"])
  end
    
  private

    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "は５MB以下でアップロードしてください")
      end
    end
end