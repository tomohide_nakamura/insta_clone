class PostsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user,   only: :destroy

  def show
    @post = Post.find(params[:id])
    @user = @post.user
    @comments = @post.comments.paginate(page: params[:page])
    @comment = Comment.new
  end

  def new
    @post = current_user.posts.build
  end
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "画像をアップロードしました！"
      redirect_to @post
    else
      @feed_items = []
      render 'new'
    end
  end
  
  def destroy
    @post.destroy
    flash[:success] = "画像を消去しました！"
    redirect_to request.referrer || root_url
  end
  
  def search
    @posts = Post.search(params[:search])
  end
  
  private

    def post_params
      params.require(:post).permit(:picture_title, :picture)
    end

    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end
