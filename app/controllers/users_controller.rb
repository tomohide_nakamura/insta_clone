class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update,
                                :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "登録が完了しました！"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "編集が完了しました！"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "ユーザーを消去しました"
    redirect_to users_url
  end
  
  def resign
    User.find(params[:id]).destroy
    flash[:success] = "退会しました"
    redirect_to root_path
  end
  
  def edit_password
    @user = User.find(params[:id])
  end
  
  def update_password
    @user = User.find(params[:id])
    if params[:user][:new_password].empty?
      flash[:danger] = "新しいパスワードを入力してください"
      render 'edit_password'
    elsif @user.authenticate(params[:user][:password]) && 
          params[:user][:new_password] == params[:user][:new_password_confirmation]
      @user.update_attributes(password: params[:new_password],
            password_confirmation: params[:new_password_confirmation])
      flash[:success] = "パスワードを変更しました！"
      redirect_to @user
    else
      flash[:danger] = "パスワードが正しくありません" unless  @user.authenticate(params[:user][:password]) 
      flash[:danger] = "新しいパスワードは同じ内容を入力してください" unless
            params[:user][:new_password] == params[:user][:new_password_confirmation]
      render 'edit_password'
    end
  end
  
  def following
    @title = "フォロー中"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "フォロワー"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :user_name, :password,
            :password_confirmation, :agreement, :email, :provider, :uid,
            :website, :introduce, :phone_number, :gender)
    end
    
    # 正しいユーザーかどうか確認
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # 管理者かどうか確認
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end