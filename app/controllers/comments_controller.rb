class CommentsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user,   only: :destroy
  
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      redirect_to @post
      flash[:success] = "コメントしました"
    else
      flash[:danger] = "コメントを入力してください"
      redirect_to @post
    end
  end
  
  def destroy
    @comment.destroy
    flash[:success] = "コメントを消去しました！"
    redirect_to request.referrer || root_url
  end

  private
  
    def comment_params
      params.require(:comment).permit(:content)
    end
  
    def correct_user
        @comment = current_user.comment.find_by(id: params[:id])
        redirect_to root_url if @comment.nil?
    end
end