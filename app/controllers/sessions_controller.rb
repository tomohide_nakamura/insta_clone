class SessionsController < ApplicationController
  
  def create
    auth = request.env["omniauth.auth"]
    @user =  User.find_by(provider: auth.provider, uid: auth.uid)
    if @user
      log_in @user
      flash[:notice] = "ユーザー認証が完了しました。"
      redirect_back_or @user
    else
      session[:provider] =auth.provider
      session[:uid] = auth.uid
      redirect_to signup_path
    end
  end
  
  def failure 
    render 'static_pages/home'
  end

  def destroy
    log_out if logged_in?
    flash[:notice] = "ログアウトしました。"
    redirect_to root_path
  end
end
