class FavoritesController < ApplicationController

  def create
    @user=current_user
    @post=Post.find(params[:post_id])
    if Favorite.create(user_id: @user.id, post_id: @post.id)
      redirect_back(fallback_location: root_path)
      flash[:success] = "お気に入りに追加しました"
    else
      redirect_back(fallback_location: root_path)
      flash[:danger] = "お気に入り失敗"
    end
  end

  def destroy
    @user=current_user
    @post=Post.find(params[:post_id])
    if @favorite=Favorite.find_by(user_id: @user.id, post_id: @post.id)
      @favorite.delete
      redirect_back(fallback_location: root_path)
      flash[:success] = "お気に入りを解除しました"
    else
      redirect_back(fallback_location: root_path)
      flash[:danger] = "お気に入り解除失敗"
    end
    
  end
end