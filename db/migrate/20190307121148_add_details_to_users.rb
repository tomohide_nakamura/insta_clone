class AddDetailsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :string
    add_column :users, :introduce, :string
    add_column :users, :phone_number, :string
    add_column :users, :gender, :integer
  end
end
