User.create!(name:  "Example User",
             user_name: "exam",
             password:              "foobar",
             password_confirmation: "foobar",
             agreement: true,
             admin: true)

99.times do |n|
  name  = Faker::Name.name
  user_name = Faker::Name.name
  password = "password"
  User.create!(name:  name,
               user_name: user_name,
               password:              password,
               password_confirmation: password,
               agreement: true)
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }