Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook,
  ENV['APP_ID'], ENV['APP_SECRET']
  
  on_failure do |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  end
end