Rails.application.routes.draw do
  root"static_pages#home" 
  get 'static_pages/home'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get '/user_policy', to: 'static_pages#user_policy', as: 'user_policy'
  get '/auth/facebook/callback', to: 'sessions#create'
  get "/auth/failure" => "sessions#failure"
  delete '/logout',  to: 'sessions#destroy'
  delete '/resign/:id', to: 'users#resign', as: 'resign'
  get '/search', to: 'posts#search', as: 'search'
  resources :users do
    member do
      get :following, :followers
    end
  end
  get '/users/:id/password', to: 'users#edit_password', as: 'edit_password'
  patch '/users/:id/password', to: 'users#update_password', as: 'password'
  resources :posts,          only: [:show, :new, :create, :destroy] do
    post '/add', to: 'favorites#create'
    delete '/add', to: 'favorites#destroy'
    resources :comments, only: [:create, :destroy]
  end
  resources :relationships,       only: [:create, :destroy]
end
